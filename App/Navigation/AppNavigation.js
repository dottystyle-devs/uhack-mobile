import React, {Component} from 'react'
import {View, StatusBar, TouchableOpacity, Text} from 'react-native';
import { Animated, Easing } from 'react-native'
import {
  createNavigator,
  createNavigationContainer,
  StackRouter,
  addNavigationHelpers,
  StackNavigator
} from 'react-navigation';
import RegisterScreen from '../Containers/RegisterScreen'
// import ScalingDrawer from 'react-native-scaling-drawer';
import TerminalOverviewScreen from '../Containers/TerminalOverviewScreen'
import GalleryScreen from '../Containers/GalleryScreen'
import CameraScreen from '../Containers/CameraScreen'
import MapScreen from '../Containers/MapScreen'
import LaunchScreen from '../Containers/LaunchScreen'


import MapComponent from '../Components/MapComponent'
import LeftMenu from '../Components/LeftMenu';

import styles from './Styles/NavigationStyles'

let defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20
};

// class CustomDrawerView extends Component {
//   constructor(props) {
//     super(props);
//   }

//   componentWillReceiveProps(nextProps) {
//     /** Active Drawer Swipe **/
//     if (nextProps.navigation.state.index === 0)
//       this._drawer.blockSwipeAbleDrawer(false);

//     if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
//       this._drawer.blockSwipeAbleDrawer(false);
//       this._drawer.close();
//     }

//     /** Block Drawer Swipe **/
//     if (nextProps.navigation.state.index > 0) {
//       this._drawer.blockSwipeAbleDrawer(true);
//     }
//   }

//   setDynamicDrawerValue = (type, value) => {
//     defaultScalingDrawerConfig[type] = value;
//     /** forceUpdate show drawer dynamic scaling example **/
//     this.forceUpdate();
//   };

//   render() {
//     const {routes, index} = this.props.navigation.state;
//     const ActiveScreen = this.props.router.getComponentForState(this.props.navigation.state);

//     return (
//       <ScalingDrawer
//         ref={ref => this._drawer = ref}
//         content={<LeftMenu navigation={this.props.navigation}/>}
//         {...defaultScalingDrawerConfig}
//         onClose={() => console.tron.log('close')}
//         onOpen={() => console.tron.log('open')}
//       >
//         <ActiveScreen
//           navigation={addNavigationHelpers({
//             ...this.props.navigation,
//             state: routes[index],
//             openDrawer: () => this._drawer.open(),
//           })}
//           dynamicDrawerValue={ (type, val) => this.setDynamicDrawerValue(type, val) }
//         />
//       </ScalingDrawer>
//     )
//   }
// }



// Manifest of possible screens
const PrimaryNav = StackNavigator({
  RegisterScreen: { screen: RegisterScreen },
  TerminalOverviewScreen: { screen: TerminalOverviewScreen },
  GalleryScreen: { screen: GalleryScreen },
  MapScreen: { screen: MapScreen },
  CameraScreen: { screen: CameraScreen },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'RegisterScreen',
  navigationOptions: {
    headerStyle: styles.header
  },
  transitionConfig: () => ({
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0
    }
  })
})

// const AppNavigator = StackRouter({
//   TerminalOverviewScreen: { screen: TerminalOverviewScreen },
//   GalleryScreen: { screen: GalleryScreen },
//   MapScreen: { screen: MapScreen },
//   CameraScreen: { screen: CameraScreen },
//   LaunchScreen: { screen: LaunchScreen },
//   RegisterScreen: { screen: RegisterScreen },
// }, {
//   initialRouteName: 'GalleryScreen',
// });



// const CustomDrawer = createNavigationContainer(createNavigator(AppNavigator)(CustomDrawerView));

// export default CustomDrawer;

export default PrimaryNav
