import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import _ from 'lodash'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  terminalRequest: ['geo'],
  terminalSuccess: ['terminals'],
  terminalFailure: null,
  terminalSetActive: ['id']
})

export const TerminalTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  terminals: null,
  fetching: null,
  error: null,
  geo: null,
  active: null
})

/* ------------- Reducers ------------- */

// request terminals
export const request = (state, { geo }) =>{
  console.log({request: geo})
  return state.merge({ fetching: true, geo, terminals: null, active: null })
  
}

// successful terminals
export const success = (state, action) => {
  const { terminals } = action
  return state.merge({ fetching: false, error: null, terminals, active: null })
}

// failed to get the terminals
export const failure = (state) =>
  state.merge({ fetching: false, error: true, terminals: null, active: null })

// set active terminal
export const setActive = (state, {id}) => {
  return state.merge({ fetching: false, error: null, active: id })
}


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TERMINAL_REQUEST]: request,
  [Types.TERMINAL_SUCCESS]: success,
  [Types.TERMINAL_FAILURE]: failure,
  [Types.TERMINAL_SET_ACTIVE]: setActive,
})
