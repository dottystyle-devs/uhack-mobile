import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  registerRequest: ['params'],
  registerSuccess: ['user'],
  registerFailure: ['error'],
  registerReset: null
})

export const RegisterTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  name: null,
  email: null,
  password: null,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to register
export const request = (state) => {
  return state.merge({ fetching: true, error: null })
}
// we've successfully register in
export const success = (state, { user }) => {
  return state.merge({ fetching: false, error: null, user })
}

// we've had a problem logging in
export const failure = (state, { error }) => {
  return state.merge({ fetching: false, error })
}

export const reset = (state) => {
  return state.merge({ fetching: false, error: 'reset' })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REGISTER_REQUEST]: request,
  [Types.REGISTER_SUCCESS]: success,
  [Types.REGISTER_FAILURE]: failure,
  [Types.REGISTER_RESET]: reset
})

/* ------------- Selectors ------------- */

