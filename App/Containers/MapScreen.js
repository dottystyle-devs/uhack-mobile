import React, { Component } from 'react'
import { ScrollView, KeyboardAvoidingView, View, Dimensions, TouchableOpacity, FlatList, } from 'react-native'
import { Container, Header, Content,Form, Input, Item, Card, CardItem, Thumbnail, Text, Button, Left, Body } from 'native-base'
import { connect } from 'react-redux'
import _ from 'lodash'

import RNGooglePlaces from 'react-native-google-places'
import Icon from 'react-native-vector-icons/FontAwesome'

// ACTIONS
import TerminalActions from '../Redux/TerminalRedux'

import MapComponent from '../Components/MapComponent'
import CardTerminalOverview from '../Components/CardTerminalOverview'

// Styles
import styles from './Styles/MapScreenStyle'

// Temporary data

const listItems = []
const tempGeo = {latitude:14.550269,longitude: 121.0048382, latitudeDelta: 0.0041,
  longitudeDelta: 0.0021}

class MapScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentGeo: {},
      destination: null,
      terminals: []
    }
  }
  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      ({ coords }) => {
        const currentGeo = {latitude: coords.latitude,
          longitude: coords.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005}
        this.setState({currentGeo})
      },
      (error) => alert('Error: Are location services on?'),
      { enableHighAccuracy: true }
    )
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.terminals.length) {
      if(nextProps.activeTerminal) {
        // const listIndex = _.findIndex(nextProps.terminals, { 'id': 'nextProps.activeTerminal' })
        const listIndex = _.findIndex(nextProps.terminals, (o) => o.id === nextProps.activeTerminal )
        console.tron.log({listIndex, terminals: nextProps.terminals})
        this.scrollToItem(listIndex)
      } else {
        // this.scrollToItem(0)
      }
    }
  }

  openSearchModal = () => {
    RNGooglePlaces.openAutocompleteModal()
    .then((place) => {
    console.tron.log({distanation: place});
    this.props.getTerminals({longitude: place.longitude, latitude: place.latitude})
    this.setState({destination: place.name})
		// place represents user's selection from the
    // suggestions and it is a simplified Google Place object.
    })
    .catch(error => console.log(error.message));  // error is a Javascript Error object

  }

  onSubmit = () => {
    // this.map.animateToCoordinate(currentGeo)
    this.setState({currentGeo:tempGeo})
  }

  scrollToItem = (index) => {
    // this.flatListRef.scrollToIndex({animated: true, index: '' + index})
    console.tron.log({index}, true)
    // this.flatListRef.scrollToIndex({index, animated: true})
    setTimeout(() => { this.flatListRef.scrollToIndex({animated:true , index: index, viewPosition: 0.5}) }, 100);
  }

  renderListRow = (rowData) =>{
    const item = rowData.item
    return (
      <View key={rowData.index} style={{marginRight:40}}>
        <CardTerminalOverview data={item} index={rowData.index}/>
      </View>
    )
  }

  render () {
    // temp values
    const headerHeight = 200

    const {currentGeo, destination} = this.state
    const {terminals,activeTerminal} = this.props
    return (
      <Container>
        <View style={styles.container}>
          <MapComponent style={styles.map} data={terminals} currentGeo={currentGeo}/>
          <View style={styles.inputContainer}>
            <Form>
              {/* <Item rounded style={styles.item}>
                <Input placeholder='Your current location' style={{paddingLeft: 15, paddingRight: 15}} />
                <Icon name='location-arrow' size={25} style={{marginRight: 15}} />
              </Item> */}
              <Item rounded style={styles.item}>
                <Input onFocus={() => this.openSearchModal()} placeholder='Where to?' style={{paddingLeft: 15, paddingRight: 15}} value={destination}/>
                <Icon name='map-marker' size={25} style={{marginRight: 15}} />
              </Item>

            </Form>
          </View>

         
          <View style={{flexDirection:'row', justifyContent: 'center', alignItems: 'center',position: 'absolute', bottom: 30}}>
            <FlatList
              data={terminals}
              extraData={terminals}
              ref={(ref) => { this.flatListRef = ref }}
              // ListEmptyComponent={this.renderEmptyList}
              // ListHeaderComponent={this.renderHeader}
              // ListFooterComponent={this.renderFooter}
              renderItem={(item) => this.renderListRow(item)}
              keyExtractor={item => item.id}
              scrollEventThrottle={16}
              removeClippedSubviews={false}
              horizontal
              pagingEnabled={true}
              snapToAlignment={'center'}  
              showsHorizontalScrollIndicator={false}
            />
          </View>
        </View>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    terminals: state.terminals.terminals,
    activeTerminal: state.terminals.active
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTerminals: (params) => dispatch(TerminalActions.terminalRequest(params)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen)
