import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import CardTerminalOverview from '../Components/CardTerminalOverview'


// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/TerminalOverviewScreenStyle'

class TerminalOverviewScreen extends Component {
  render () {
    return (
      <View style={{position:'relative', top:100}}>
        <CardTerminalOverview />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TerminalOverviewScreen)
