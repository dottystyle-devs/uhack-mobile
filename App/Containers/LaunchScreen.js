import React, { Component } from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import { Images } from '../Themes'
import { Container, Header, Content, Button, Text as NBText } from 'native-base'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default class LaunchScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
          </View>

          <View style={styles.section} >
            <Button block><NBText>U-Hack!</NBText></Button>
            <Text style={styles.sectionText}>
            Lorem ipsum dolor sit amet, eu oblique singulis mea, ei democritum disputando his. Eam ei quis nobis, ut quo simul moderatius liberavisse. Cibo fabellas ne mei. Vim ex libris vocibus senserit, vel semper omittam adolescens ei.
            </Text>
          </View>

        </ScrollView>
      </View>
    )
  }
}
