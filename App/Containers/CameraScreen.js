import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Container, Header, Left, Body, Right, Button, Title, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import CameraKit from '../Components/CameraKit'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
// import styles from './Styles/CameraScreenStyle'

class CameraScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  handlerNavigation (screen) {
    const { navigate } = this.props.navigation
    navigate(screen)
  }

  renderHeader () {
    return (
      <Header style={{backgroundColor:'#000', borderBottomWidth:0}}>
        <Left style={{ flex: 1 }}>
          <Button transparent>
            <MaterialIcons name='chevron-left' style={{ fontSize: 30, color: '#fff' }} />
          </Button>
        </Left>
        <Body style={{ alignItems: 'center', flex: 1 }}>
          <Title style={{ textAlign: 'center' }}>
            Camera
          </Title>
        </Body>
        <Right style={{ flex: 1 }} />
      </Header>
    )
  }

  renderFooter () {
    return (
      <Footer>
        <FooterTab>
          <Button onPress={() => {this.handlerNavigation('GalleryScreen')}}>
            <Text style={{ color: '#fff', fontSize: 16, fontWeight: '600' }}>
              Library
            </Text>
          </Button>
          <Button active style={{ opacity: 0.5 }}>
            <Text style={{ color: '#fff', fontSize: 16, fontWeight: '600' }}>
              Camera
            </Text>
          </Button>
        </FooterTab>
      </Footer>
    )
  }

  renderCamera () {
    return (
      <CameraKit />
    )
  }

  render () {
    return (
      <Container>
        {this.renderHeader()}
        {this.renderCamera()}
        {/* {this.renderFooter()} */}
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CameraScreen)
