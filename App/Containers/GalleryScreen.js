import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Container, Header, Left, Body, Right, Button, Title, Footer, FooterTab } from 'native-base'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import CameraKit from '../Components/CameraKit'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/GalleryScreenStyle'

class GalleryScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  handlerNavigation (screen) {
    const { navigate } = this.props.navigation
    navigate(screen)
  }

  renderHeader () {
    return (
      <Header>
        <Left style={{ flex: 1 }}>
          <Button transparent>
            <MaterialIcons name='chevron-left' style={{ fontSize: 30, color: '#fff' }} />
          </Button>
        </Left>
        <Body style={{ alignItems: 'center', flex: 1 }}>
          <Title style={{ textAlign: 'center' }}>
            Gallery
          </Title>
        </Body>
        <Right style={{ flex: 1 }} />
      </Header>
    )
  }

  renderFooter () {
    return (
      <Footer>
        <FooterTab>
          <Button active  style={{ opacity: 0.5 }}>
            <Text style={{ color: '#fff', fontSize: 16, fontWeight: '600' }}>
              Library
            </Text>
          </Button>
          <Button  onPress={() => {this.handlerNavigation('CameraScreen')}}>
            <Text style={{ color: '#fff', fontSize: 16, fontWeight: '600' }}>
              Camera
            </Text>
          </Button>
        </FooterTab>
      </Footer>
    )
  }

  renderCamera () {
    return (
      <CameraKit />
    )
  }

  render () {
    return (
      <Container>
        {this.renderHeader()}
        {this.renderCamera()}
        {this.renderFooter()}
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryScreen)
