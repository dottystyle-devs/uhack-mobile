import React, { Component } from 'react'
import { ScrollView, KeyboardAvoidingView } from 'react-native'
import { connect } from 'react-redux'
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Toast } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'

import RegisterActions from '../Redux/RegisterRedux'

// Styles
import styles from './Styles/RegisterScreenStyle'

class RegisterScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      password: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!nextProps.fetching) {
      if(nextProps.error) {
        Toast.show({
          text: nextProps.error,
          position: 'bottom',
          buttonText: 'Okay',
          duration: 3000,
          // type: 'success'
        })
      }
    }
  }

  handleChangeName = text => {
    this.setState({ name: text })
  }

  handleChangeEmailAddress = text => {
    this.setState({ email: text })
  }

  handleChangePassword = text => {
    this.setState({ password: text })
  }

  register = () => {
    // Toast.show({
    //   text: 'Wrong password!',
    //   position: 'bottom',
    //   buttonText: 'Okay'
    // })
    const param = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
    }
    this.props.attemptRegister(param)
  }

  render () {
    const { name, email, password} = this.state
    return (
      <Container>
      <Header />
      <Content style={{paddingHorizontal: 15, paddingVertical: 15}}>
        <Form>
          <Item floatingLabel>
            <Label>Name</Label>
            <Input
              ref={(ref) => { this.name = ref }}
              // placeholder='Name'
              value={name}
              // editable={editable}
              keyboardType='default'
              returnKeyType='next'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={this.handleChangeName}
              underlineColorAndroid='transparent'
              onSubmitEditing={() => this.email._root.focus()} />
            <Icon active name='user' />
          </Item>
          <Item floatingLabel last>
            <Label>Email Address</Label>
            <Input
             ref={(ref) => { this.email = ref }}
            //  placeholder='Email address'
             value={email}
            //  editable={editable}
             keyboardType='email-address'
             returnKeyType='done'
             autoCapitalize='none'
             autoCorrect={false}
             onChangeText={this.handleChangeEmailAddress}
             underlineColorAndroid='transparent'
             onSubmitEditing={() => this.password._root.focus()}  />
            <Icon active name='envelope-o' />
          </Item>
          <Item floatingLabel last>
            <Label>Password</Label>
          <Input
            ref={(ref) => { this.password = ref }}
            value={password}
            // editable={editable}
            keyboardType='default'
            returnKeyType='go'
            autoCapitalize='none'
            secureTextEntry
            autoCorrect={false}
            onChangeText={this.handleChangePassword}
            underlineColorAndroid='transparent'
            onSubmitEditing={() => this.register()} />
             <Icon active name='key' />
          </Item>
          <Button onPress={() => this.register()} block style={{marginVertical: 50}} ><Text>Register</Text></Button>
        </Form>
      </Content>
    </Container>

      // <ScrollView style={styles.container}>
      //   <KeyboardAvoidingView behavior='position'>
      //     <Text>RegisterScreen</Text>
      //   </KeyboardAvoidingView>
      // </ScrollView>
      
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.register.fetching,
    error: state.register.error,
    user: state.register.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptRegister: (params) => dispatch(RegisterActions.registerRequest(params)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen)
