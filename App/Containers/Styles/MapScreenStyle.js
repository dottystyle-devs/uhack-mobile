import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-start',
    // alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  inputContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginTop: 80,
    paddingHorizontal: Metrics.marginHorizontal
    // borderColor: '#4CAF50',
    // borderWidth: 1
  },
  item: {
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 10,
    // height: 50,
    height: 60,
    marginBottom: Metrics.marginVertical
  }

})
