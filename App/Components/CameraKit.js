import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { View, Text, Alert } from 'react-native'
import { CameraKitCameraScreen } from './PhotoKit'
import styles from './Styles/CameraKitStyle'

export default class CameraKit extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  onBottomButtonPressed (event) {
    const vm = this
    const captureImages = JSON.stringify(event.captureImages)
    const data = {
      image: event.captureImages[event.captureImages.length - 1].uri.replace('file://', ''),
      screen: 'CameraScreen'
    }

    console.log(data)
  }

  render () {
    return (

      <CameraKitCameraScreen
        onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
        flashImages={{ on: require('../Images/Camera/flashOn.png'), off: require('../Images/Camera/flashOff.png'), auto: require('../Images/Camera/flashAuto.png') }}
        cameraFlipImage={require('../Images/Camera/cameraFlipIcon.png')}
        captureButtonImage={require('../Images/Camera/cameraButton.png')} />
    )
  }
}
