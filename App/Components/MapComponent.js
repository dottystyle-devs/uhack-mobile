import React from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import MapView from 'react-native-maps'
import MapComponentCallout from './MapComponentCallout'
import Styles from './Styles/MapComponentStyles'

import TerminalActions from '../Redux/TerminalRedux'
import Images from '../Themes/Images'

// Generate this MapHelpers file with `ignite generate map-utilities`
// You must have Ramda as a dev dependency to use this.
// import { calculateRegion } from '../Lib/MapHelpers'


const DEFAULT_PADDING = { top: 50, right: 50, bottom: 50, left: 50 }

class MapComponent extends React.Component {
  /* ***********************************************************
  * This generated code is only intended to get you started with the basics.
  * There are TONS of options available from traffic to buildings to indoors to compass and more!
  * For full documentation, see https://github.com/lelandrichardson/react-native-maps
  *************************************************************/

  constructor (props) {
    super(props)
    /* ***********************************************************
    * STEP 1
    * Set the array of locations to be displayed on your map. You'll need to define at least
    * a latitude and longitude as well as any additional information you wish to display.
    *************************************************************/
    const locations = [
      // { title: 'Location A', latitude: 121.0059113, longitude: 14.557381 },
      // { title: 'Location B', latitude: 120.9950043, longitude: 14.554948 }
    ]
    /* ***********************************************************
    * STEP 2
    * Set your initial region either by dynamically calculating from a list of locations (as below)
    * or as a fixed point, eg: { latitude: 123, longitude: 123, latitudeDelta: 0.1, longitudeDelta: 0.1}
    * You can generate a handy `calculateRegion` function with
    * `ignite generate map-utilities`
    *************************************************************/
    // const region = calculateRegion(locations, { latPadding: 0.05, longPadding: 0.05 })
    const region = { latitude: 123, longitude: 123, latitudeDelta: 0.1, longitudeDelta: 0.1}
    this.state = {
      region,
      locations,
      showUserLocation: true
    }
    this.renderMapMarkers = this.renderMapMarkers.bind(this)
    this.onRegionChange = this.onRegionChange.bind(this)
  }

  componentWillReceiveProps (newProps) {
    /* ***********************************************************
    * STEP 3
    * If you wish to recenter the map on new locations any time the
    * props change, do something like this:
    *************************************************************/
    // this.setState({
    //   region: calculateRegion(newProps.data, { latPadding: 0.1, longPadding: 0.1 })
    // })

    console.tron.log({locations: newProps.data})

    if(newProps.data) {
      this.setState({locations: newProps.data})
      this.fitAllMarkers(newProps.data)
    }


    if(newProps.currentGeo) {
      if(this.props.currentGeo !== newProps.currentGeo) {
        // this.map.animateToCoordinate(newProps.currentGeo)
        this.map.animateToRegion({
          latitude: newProps.currentGeo.latitude,
          longitude: newProps.currentGeo.longitude,
          latitudeDelta: 0.020,
          longitudeDelta: 0.020
        })
      }
    }

    
  }

  fitAllMarkers = (locations) => {

    this.map.fitToCoordinates(locations, {
      edgePadding: (locations.length === 1) ? { top: 0, right: 0, bottom: 0, left: 0 } : DEFAULT_PADDING,
      animated: true
    })
  }

  onRegionChange (newRegion) {
    /* ***********************************************************
    * STEP 4
    * If you wish to fetch new locations when the user changes the
    * currently visible region, do something like this:
    *************************************************************/
    // const searchRegion = {
    //   ne_lat: newRegion.latitude + newRegion.latitudeDelta / 2,
    //   ne_long: newRegion.longitude + newRegion.longitudeDelta / 2,
    //   sw_lat: newRegion.latitude - newRegion.latitudeDelta / 2,
    //   sw_long: newRegion.longitude - newRegion.longitudeDelta / 2
    // }
    // Fetch new data...
  }

  calloutPress (location) {
    /* ***********************************************************
    * STEP 5
    * Configure what will happen (if anything) when the user
    * presses your callout.
    *************************************************************/
    
    console.tron.log({location}) // Reactotron
  }

  renderMapMarkers =  (location) => {
    /* ***********************************************************
    * STEP 6
    * Customize the appearance and location of the map marker.
    * Customize the callout in ./MapComponentCallout.js
    *************************************************************/


    return (
      <MapView.Marker onPress={()=> this.props.setActiveTerminal(location.id)}
        image={Images.bus}
        key={location.title}
        coordinate={{latitude: location.latitude, longitude: location.longitude}}>
        {/* <MapComponentCallout location={location} onPress={this.calloutPress} /> */}
      </MapView.Marker>
    )
  }

  render () {
    return (
      <MapView
        ref={ref => { this.map = ref }}
        style={Styles.map}
        initialRegion={this.state.region}
        onRegionChangeComplete={this.onRegionChange}
        showsUserLocation={this.state.showUserLocation}
        showUserLocation
        showsPointsOfInterest={false}
      >
        {this.state.locations.map((location) => this.renderMapMarkers(location))}
      </MapView>
    )
  }
}

// export default MapComponent

const mapStateToProps = (state) => {
  return {
    terminals: state.terminals.terminals,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setActiveTerminal: (id) => dispatch(TerminalActions.terminalSetActive(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapComponent)


