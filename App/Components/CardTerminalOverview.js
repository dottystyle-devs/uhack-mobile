import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, Dimensions } from 'react-native'
import { Container, Header, Left, Body, Right, Button } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient'
const { width, height } = Dimensions.get('window')
//#0095d3 - blue
//#c3c3c3 - grey
export default class CardTerinalOverview extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: this.props.index,
      data : this.props.data
    }

    console.log(this.state)
  }
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  renderHeader () {
    const { data } = this.state
    return (
      <View style={{flexDirection:'row', paddingVertical:10, borderBottomWidth:1, borderColor:'#c3c3c3'}}>
        <Left style={{flex:0, paddingHorizontal:10}}>
          <View style={{height:30, width:30, overflow:'hidden', justifyContent:'center', alignItems:'center', borderRadius:30, borderWidth:1, borderColor:'#c3c3c3'}}>
            <Icon name='bus' style={{fontSize:16, color:'#0095d3', alignSelf:'center'}} />
          </View>
        </Left>
        <Body style={{flex:1, alignItems:'flex-start', paddingRight:10}}>
          <View>
            <Text style={{fontSize:18, fontWeight:'100', color:'#0095d3'}}>{data ? data.name ? data.name : null : null}</Text>
            <Text style={{fontSize:12, color:'#c3c3c3'}}>{data ? data.address ? data.address : null : null}</Text>
          </View>
        </Body>
      </View>
    )
  }

  renderMeter () {
    const { data } = this.state
    return (
      <View style={{alignItems:'center', justifyContent:'center', paddingVertical:10, flex:1}}>
        <View style={{marginBottom:10}}>
          <LinearGradient 
            start={{x: 0.0, y: 0.0}} end={{x: 1, y: 0}}
            locations={data.mood}
            colors={['#75cf00', '#ffa406', '#ff0101']} 
            style={{height:30, width:240, justifyContent:'center', alignItems:'center',borderRadius: 5}}
          >
            <Text style={{backgroundColor:'transparent', margin:10, color:'#fff'}}>
              {data.status}
            </Text>
          </LinearGradient>
        </View>
        <View style={{justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
            <Icon name='group' style={{color:'#c3c3c3', fontSize:16, position:'relative', marginRight:5}} />
            <Text style={{fontSize:12, color:'#c3c3c3', position:'relative'}}>Approx. 10 Passengers</Text>
        </View>
      </View>
    )
  }
  
  renderCardDetails () {
    return (
      <View style={{width:270, height:140, backgroundColor:'#fff', borderRadius:15}}>
          {this.renderHeader()}
          {this.renderMeter()}
      </View>
    )
  }

  renderCardButton () {
    return (
      <View style={{paddingHorizontal:10, marginTop:10}}>
        <Button full style={{backgroundColor:'#0095d3', borderRadius:5}}>
          <Text style={{color:'#fff', fontSize:16, fontWeight:'100'}}>Check Status</Text>
        </Button>
      </View>
    )
  }

  render () {
    const { index } = this.state
    return (
      <View style={{width:270, marginLeft: index === 0 ? 40 : 0}}>
        {this.renderCardDetails()}
        {this.renderCardButton()}
      </View>
    )
  }
}
