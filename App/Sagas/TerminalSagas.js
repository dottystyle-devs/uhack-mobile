import { call, put } from 'redux-saga/effects'
import TerminalActions from '../Redux/TerminalRedux'

export function * getTerminals (api, action) {
  const { geo } = action
  // make the call to the api
  const response = yield call(api.getTerminals, geo)
  console.log({response})

  if (response.ok) {
    yield put(TerminalActions.terminalSuccess(response.data.terminals))
  } else {
    yield put(TerminalActions.terminalFailure())
  }
}
