import { put, call, select } from 'redux-saga/effects'
import RegisterActions from '../Redux/RegisterRedux'

import API from '../Services/Api'

const tAPI = API.terminusAPI()
// attempts to register
export function * register (api, action) {


  const user = action.params


  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

   
  if (user.name === '' && user.email === '' &&  user.password === '') {
    yield put(RegisterActions.registerFailure('All fields Required'))
  } else if (user.name === '') {
    yield put(RegisterActions.registerFailure('Name Required'))
  } else if (user.password === '') {
    yield put(RegisterActions.registerFailure('Password Required'))
  } else if (user.email === '') {
    yield  put(RegisterActions.registerFailure('Email Required'))
  } else if (reg.test(user.email) === false) {
    yield put(RegisterActions.registerFailure('Invalid Email'))
  } else {
    try {
      const result = yield call(tAPI.register, user)
      console.tron.log({result})

      // if (result.ok) {
      //   yield put(RegisterActions.registerSuccess(result.data))
      // } else {
      //   if (result.problem === 'NETWORK_ERROR') {
      //     yield put(RegisterActions.registerFailure('Network error. Please try again after some time.'))
      //   } else {
      //     yield put(RegisterActions.registerFailure('Error occured, please try again later.'))
      //   }
      // }
    } catch (e) {
      yield put(RegisterActions.registerFailure(e))
    }
  }
}
